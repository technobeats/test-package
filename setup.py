from setuptools import setup

setup(
    name='test-package',
    packages=['cfgm_common', 'discoveryclient', 'libpartition', 'pysandesh', 'sandesh_common','vnc_api'],
    description='Hello world enterprise edition',
    version='0.1',
    url='http://github.com/example/linode_example',
    author='Jelle',
    author_email='docs@linode.com',
    keywords=['pip','linode','example']
    )