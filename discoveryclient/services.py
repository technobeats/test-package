"""
This module defines the classes for types defined in :doc:`services.xsd`
"""
import json
class Xmppservice(object):
    """
    XmppService class definition from :doc:`services.xsd`
    """
    subclass = None
    superclass = None
    service_type = 'Xmppservice'
    token = None
    def __init__(self, ip_address=None, port=None, **kwargs):
        self.ip_address = ip_address
        self.port = port
    def factory(*args_, **kwargs_):
        if Xmppservice.subclass:
            return Xmppservice.subclass(*args_, **kwargs_)
        else:
            return Xmppservice(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ip_address(self): return self.ip_address
    def set_ip_address(self, ip_address): self.ip_address = ip_address
    def get_port(self): return self.port
    def set_port(self, port): self.port = port
    def exportDict(self, name_='Xmppservice'):
        obj_json = json.dumps(self, default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
        obj_dict = json.loads(obj_json)
        return {name_ : obj_dict}
    def exportDict2(self, name_='Xmppservice'):
        obj_dict = {
            "ip-address" : self.ip_address,
            "port" : self.port,
        }
        return (name_, obj_dict)
# end class Xmppservice


class Ifmapservice(object):
    """
    IFMapService class definition from :doc:`services.xsd`
    """
    subclass = None
    superclass = None
    service_type = 'Ifmapservice'
    token = None
    def __init__(self, ip_address=None, port=None, **kwargs):
        self.ip_address = ip_address
        self.port = port
    def factory(*args_, **kwargs_):
        if Ifmapservice.subclass:
            return Ifmapservice.subclass(*args_, **kwargs_)
        else:
            return Ifmapservice(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ip_address(self): return self.ip_address
    def set_ip_address(self, ip_address): self.ip_address = ip_address
    def get_port(self): return self.port
    def set_port(self, port): self.port = port
    def exportDict(self, name_='Ifmapservice'):
        obj_json = json.dumps(self, default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
        obj_dict = json.loads(obj_json)
        return {name_ : obj_dict}
    def exportDict2(self, name_='Ifmapservice'):
        obj_dict = {
            "ip-address" : self.ip_address,
            "port" : self.port,
        }
        return (name_, obj_dict)
# end class Ifmapservice


class Apiservice(object):
    """
    ApiService class definition from :doc:`services.xsd`
    """
    subclass = None
    superclass = None
    service_type = 'Apiservice'
    token = None
    def __init__(self, ip_address=None, port=None, **kwargs):
        self.ip_address = ip_address
        self.port = port
    def factory(*args_, **kwargs_):
        if Apiservice.subclass:
            return Apiservice.subclass(*args_, **kwargs_)
        else:
            return Apiservice(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ip_address(self): return self.ip_address
    def set_ip_address(self, ip_address): self.ip_address = ip_address
    def get_port(self): return self.port
    def set_port(self, port): self.port = port
    def exportDict(self, name_='Apiservice'):
        obj_json = json.dumps(self, default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
        obj_dict = json.loads(obj_json)
        return {name_ : obj_dict}
    def exportDict2(self, name_='Apiservice'):
        obj_dict = {
            "ip-address" : self.ip_address,
            "port" : self.port,
        }
        return (name_, obj_dict)
# end class Apiservice


class Bgpservice(object):
    """
    BgpService class definition from :doc:`services.xsd`
    """
    subclass = None
    superclass = None
    service_type = 'Bgpservice'
    token = None
    def __init__(self, ip_address=None, port=None, **kwargs):
        self.ip_address = ip_address
        self.port = port
    def factory(*args_, **kwargs_):
        if Bgpservice.subclass:
            return Bgpservice.subclass(*args_, **kwargs_)
        else:
            return Bgpservice(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ip_address(self): return self.ip_address
    def set_ip_address(self, ip_address): self.ip_address = ip_address
    def get_port(self): return self.port
    def set_port(self, port): self.port = port
    def exportDict(self, name_='Bgpservice'):
        obj_json = json.dumps(self, default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
        obj_dict = json.loads(obj_json)
        return {name_ : obj_dict}
    def exportDict2(self, name_='Bgpservice'):
        obj_dict = {
            "ip-address" : self.ip_address,
            "port" : self.port,
        }
        return (name_, obj_dict)
# end class Bgpservice


class Collectorservice(object):
    """
    CollectorService class definition from :doc:`services.xsd`
    """
    subclass = None
    superclass = None
    service_type = 'Collectorservice'
    token = None
    def __init__(self, ip_address=None, port=None, **kwargs):
        self.ip_address = ip_address
        self.port = port
    def factory(*args_, **kwargs_):
        if Collectorservice.subclass:
            return Collectorservice.subclass(*args_, **kwargs_)
        else:
            return Collectorservice(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ip_address(self): return self.ip_address
    def set_ip_address(self, ip_address): self.ip_address = ip_address
    def get_port(self): return self.port
    def set_port(self, port): self.port = port
    def exportDict(self, name_='Collectorservice'):
        obj_json = json.dumps(self, default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
        obj_dict = json.loads(obj_json)
        return {name_ : obj_dict}
    def exportDict2(self, name_='Collectorservice'):
        obj_dict = {
            "ip-address" : self.ip_address,
            "port" : self.port,
        }
        return (name_, obj_dict)
# end class Collectorservice


class Servicestype(object):
    """
    ServicesType class definition from :doc:`services.xsd`
    """
    subclass = None
    superclass = None
    service_type = 'Servicestype'
    token = None
    def __init__(self, ifmap_server=None, xmpp_server=None, bgp_server=None, collector_server=None, api_server=None, **kwargs):
        if isinstance(ifmap_server, dict):
            obj = IFMapService(**ifmap_server)
            self.ifmap_server = obj
        else:
            self.ifmap_server = ifmap_server
        if isinstance(xmpp_server, dict):
            obj = XmppService(**xmpp_server)
            self.xmpp_server = obj
        else:
            self.xmpp_server = xmpp_server
        if isinstance(bgp_server, dict):
            obj = BgpService(**bgp_server)
            self.bgp_server = obj
        else:
            self.bgp_server = bgp_server
        if isinstance(collector_server, dict):
            obj = CollectorService(**collector_server)
            self.collector_server = obj
        else:
            self.collector_server = collector_server
        if isinstance(api_server, dict):
            obj = ApiService(**api_server)
            self.api_server = obj
        else:
            self.api_server = api_server
    def factory(*args_, **kwargs_):
        if Servicestype.subclass:
            return Servicestype.subclass(*args_, **kwargs_)
        else:
            return Servicestype(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ifmap_server(self): return self.ifmap_server
    def set_ifmap_server(self, ifmap_server): self.ifmap_server = ifmap_server
    def get_xmpp_server(self): return self.xmpp_server
    def set_xmpp_server(self, xmpp_server): self.xmpp_server = xmpp_server
    def get_bgp_server(self): return self.bgp_server
    def set_bgp_server(self, bgp_server): self.bgp_server = bgp_server
    def get_collector_server(self): return self.collector_server
    def set_collector_server(self, collector_server): self.collector_server = collector_server
    def get_api_server(self): return self.api_server
    def set_api_server(self, api_server): self.api_server = api_server
    def exportDict(self, name_='Servicestype'):
        obj_json = json.dumps(self, default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
        obj_dict = json.loads(obj_json)
        return {name_ : obj_dict}
    def exportDict2(self, name_='Servicestype'):
        obj_dict = {
            "ifmap-server" : self.ifmap_server,
            "xmpp-server" : self.xmpp_server,
            "bgp-server" : self.bgp_server,
            "collector-server" : self.collector_server,
            "api-server" : self.api_server,
        }
        return (name_, obj_dict)
# end class Servicestype



__all__ = [
    "Apiservice",
    "Bgpservice",
    "Collectorservice",
    "Ifmapservice",
    "Servicestype",
    "Xmppservice"
    ]
